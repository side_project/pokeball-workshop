export enum Target {
  TOP = 'top',
  MIDDLE = 'middle',
  BOTTOM = 'bottom',
}

export enum TopShell {
  BASE = 'base',
  GOLD = 'gold',
  SUPER = 'super',
  ULTRA = 'ultra',
  MASTER = 'master',
}

export enum BtnColor {
  GREEN = 'green',
  ORANGE = 'orange',
  CYAN = 'cyan',
  PURPLE = 'purple',
  YELLOW = 'yellow',
}

