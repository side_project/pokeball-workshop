import { defineStore } from 'pinia';
import { BtnColor, Target, TopShell } from '../types/main.type';

interface State {
  target?: `${Target}`;
  topShell: `${TopShell}`;
  btnColor: `${BtnColor}`;
  decalUrl?: string;
}

export const useMainStore = defineStore('main', {
  state: (): State => ({
    target: undefined,
    topShell: 'base',
    btnColor: 'green',
    decalUrl: undefined,
  }),
  actions: {
    setTarget(name?: `${Target}`) {
      this.target = name;
    },
    setTopShell(name: `${TopShell}`) {
      this.topShell = name;
    },
    setBtnColor(name: `${BtnColor}`) {
      this.btnColor = name;
    },
    setDecal(url?: string) {
      this.decalUrl = url;
    },
  }
})