import { Animation, Color3, EasingFunction, QuarticEase, Vector3 } from "@babylonjs/core";
import { defaults, get } from "lodash-es";

const createAnimationDefaultOptions = {
  frameRate: 60,
  speedRatio: 1,
}
export function createAnimation(target: any, property: string, to: number | Vector3 | Color3, option?: Partial<typeof createAnimationDefaultOptions>) {
  const {
    frameRate, speedRatio
  } = defaults(option, createAnimationDefaultOptions);

  const keys = [
    {
      frame: 0, value: get(target, property),
    },
    {
      frame: frameRate, value: to,
    },
  ];

  let animationType = Animation.ANIMATIONTYPE_FLOAT;
  if (typeof to === 'number') {
    animationType = Animation.ANIMATIONTYPE_FLOAT
  } else if ('x' in to) {
    animationType = Animation.ANIMATIONTYPE_VECTOR3
  } else if ('r' in to) {
    animationType = Animation.ANIMATIONTYPE_COLOR3
  }

  const ease = new QuarticEase();
  ease.setEasingMode(EasingFunction.EASINGMODE_EASEINOUT);

  const animation = Animation.CreateAnimation(
    property,
    animationType,
    frameRate * speedRatio,
    ease
  );
  animation.setKeys(keys);
  return { animation, frameRate };
}