import {
  ArcRotateCamera, BackgroundMaterial, Color3, Color4,
  Engine, MeshBuilder, Scene,
  SceneLoader, Animation,
  StandardMaterial, Vector3, IAnimationKey, KeyboardEventTypes, DefaultRenderingPipeline, Camera, CubicEase, EasingFunction, QuarticEase
} from '@babylonjs/core';
import { Target } from '../types/main.type';

const defaultPosition = {
  alpha: Math.PI / 3 * 2 - Math.PI * 2,
  beta: 1.198,
  radius: 5,
  target: new Vector3(0, 0.83, 0),
}
const positionList = [
  {
    name: Target.TOP,
    alpha: 2.125,
    beta: 1.112,
    radius: 6.540,
    target: new Vector3(-0.175, 0.970, 0.163),
  },
  {
    name: Target.MIDDLE,
    alpha: 1.550,
    beta: 1.593,
    radius: 4.5184,
    target: new Vector3(-0.033, 0.933, 0),
  },
  {
    name: Target.BOTTOM,
    alpha: 1.997,
    beta: 2.709,
    radius: 7.0197,
    target: new Vector3(0.303, 2.294, -0.755),
  },
]

export class BallCamera {
  readonly scene: Scene;
  readonly camera: ArcRotateCamera;
  private readonly FRAMES_PER_SECOND = 60;
  private readonly IDLE_ROTATION_SPEED = 0.1;

  constructor(name: string, scene: Scene) {
    this.camera = new ArcRotateCamera(
      name,
      defaultPosition.alpha,
      defaultPosition.beta,
      defaultPosition.radius,
      defaultPosition.target,
      scene
    );

    this.camera.wheelPrecision = 100;

    this.camera.useAutoRotationBehavior = true;
    if (this.camera.autoRotationBehavior) {
      this.camera.autoRotationBehavior.idleRotationSpeed = this.IDLE_ROTATION_SPEED;
    }

    this.scene = scene;
  }

  private createAnimation(property: string, to: number | Vector3, speedRatio = 1) {
    const path = property as keyof ArcRotateCamera;

    const keys = [
      {
        frame: 0, value: this.camera[path],
      },
      {
        frame: 60, value: to,
      },
    ];

    const animationType = typeof to === 'number'
      ? Animation.ANIMATIONTYPE_FLOAT
      : Animation.ANIMATIONTYPE_VECTOR3;

    const ease = new QuarticEase();
    ease.setEasingMode(EasingFunction.EASINGMODE_EASEINOUT);

    const animation = Animation.CreateAnimation(
      property,
      animationType,
      this.FRAMES_PER_SECOND * speedRatio,
      ease
    );
    animation.setKeys(keys);
    return animation;
  }

  move(name?: `${Target}`, speedRatio = 1) {
    let data: typeof defaultPosition;

    this.camera.useAutoRotationBehavior = !name;
    if (this.camera.autoRotationBehavior) {
      this.camera.autoRotationBehavior.idleRotationSpeed = this.IDLE_ROTATION_SPEED;
    }

    if (!name) {
      data = defaultPosition;
    } else {
      const target = positionList.find((item) => item.name === name);
      if (!target) {
        return Promise.reject(`目標不存在`);
      }

      data = target;
    }

    const { radius, alpha, beta, target } = data;

    // 避免自動旋轉，轉太多圈
    this.camera.alpha = this.camera.alpha % (Math.PI * 2)

    this.camera.animations = [
      this.createAnimation('alpha', alpha, speedRatio),
      this.createAnimation('beta', beta, speedRatio),
      this.createAnimation('radius', radius, speedRatio),
      this.createAnimation('target', target, speedRatio),
    ];

    this.scene.beginAnimation(this.camera, 0, this.FRAMES_PER_SECOND, false);
  }
}
