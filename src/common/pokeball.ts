import {
  Scene, Color3, Vector3,
  SceneLoader, AbstractMesh, AnimationGroup,
  MeshBuilder, PhysicsImpostor, StandardMaterial,
  Animation, ActionManager, InterpolateValueAction, EasingFunction, CircleEase, QuarticEase, Color4, GlowLayer, DynamicTexture, Texture, Mesh, PointerEventTypes, PickingInfo, PointerInfo,
} from '@babylonjs/core';
import { get, set, throttle } from 'lodash-es';
import { BtnColor, TopShell } from '../types/main.type';

import defaultDecal from '../assets/decal-pokemon.png';

interface AnimationMap {
  rise: Animation,
  fall: Animation,
}

const topShellList = [
  {
    name: TopShell.BASE,
    path: 'pokeball-top-base.glb',
  },
  {
    name: TopShell.GOLD,
    path: 'pokeball-top-gold.glb',
  },
  {
    name: TopShell.SUPER,
    path: 'pokeball-top-super.glb',
  },
  {
    name: TopShell.ULTRA,
    path: 'pokeball-top-ultra.glb',
  },
  {
    name: TopShell.MASTER,
    path: 'pokeball-top-master.glb',
  },
];


const btnColorList = [
  {
    name: BtnColor.GREEN,
    diffuseColor: new Color3(0, 0.5, 0),
    emissiveColor: new Color3(0.4, 1, 0.4),
  },
  {
    name: BtnColor.ORANGE,
    diffuseColor: new Color3(0.8, 0.47, 0.03),
    emissiveColor: new Color3(1, 0.46, 0.19),
  },
  {
    name: BtnColor.CYAN,
    diffuseColor: new Color3(0, 0.5, 0.5),
    emissiveColor: new Color3(0.4, 1, 0.8),
  },
  {
    name: BtnColor.PURPLE,
    diffuseColor: new Color3(0.91, 0.13, 0.98),
    emissiveColor: new Color3(0.82, 0.32, 0.76),
  },
  {
    name: BtnColor.YELLOW,
    diffuseColor: new Color3(0.28, 0.28, 0),
    emissiveColor: new Color3(0.6, 0.6, 0),
  },
];

export class PokeBall {
  name: string;
  scene: Scene;

  buttonMesh!: AbstractMesh;
  buttonMaterial!: StandardMaterial;

  targetShell?: `${TopShell}`;
  topMeshes: {
    name: string;
    mesh: AbstractMesh;
  }[] = [];
  isChanging = false;

  private canDecal = false;
  private decalMaterial!: StandardMaterial;
  private decal?: Mesh;
  private decalAngle = 0;
  private decalSize = 0.5;

  private animationMap!: AnimationMap;

  private readonly FRAME_RATE = 10;
  private readonly TOP_SHELL_MOVE_HEIGHT = 6;

  constructor(name: string, scene: Scene) {
    this.name = name;
    this.scene = scene;
    this.initTopShellAnimation();
    this.initDecal();
  }

  async init() {
    // 初始化按鈕
    const bottomResult = await SceneLoader.ImportMeshAsync(
      '', `model/`, 'pokeball-bottom.glb', this.scene
    );
    this.buttonMesh = bottomResult.meshes[0];

    const buttonMesh = bottomResult.meshes[2];
    this.buttonMaterial = new StandardMaterial('buttonMaterial', this.scene);

    this.setButtonColor('green');
    buttonMesh.material = this.buttonMaterial;

    // 初始化外殼
    const tasks = topShellList.map(({ path }) =>
      SceneLoader.ImportMeshAsync('', `model/`, path, this.scene)
    );

    const results = await Promise.allSettled(tasks);
    results.forEach((result, i) => {
      if (result.status !== 'fulfilled') return;

      const name = topShellList[i].name;
      const mesh = result.value.meshes[0];

      if (name !== 'base') {
        mesh.setEnabled(false);
      }

      this.topMeshes.push({
        name, mesh,
      });
    });

    return this;
  }
  private createAnimation(target: any, property: string, to: number | Vector3 | Color3) {
    const keys = [
      {
        frame: 0, value: get(target, property),
      },
      {
        frame: this.FRAME_RATE, value: to,
      },
    ];

    let animationType = Animation.ANIMATIONTYPE_FLOAT;
    if (typeof to === 'number') {
      animationType = Animation.ANIMATIONTYPE_FLOAT
    } else if ('x' in to) {
      animationType = Animation.ANIMATIONTYPE_VECTOR3
    } else if ('r' in to) {
      animationType = Animation.ANIMATIONTYPE_COLOR3
    }

    const ease = new QuarticEase();
    ease.setEasingMode(EasingFunction.EASINGMODE_EASEINOUT);

    const animation = Animation.CreateAnimation(
      property,
      animationType,
      this.FRAME_RATE,
      ease
    );
    animation.setKeys(keys);
    return animation;
  }

  private initDecal() {
    this.decalMaterial = new StandardMaterial('decalMaterial', this.scene);
    this.decalMaterial.zOffset = -2;

    this.decalMaterial.diffuseTexture = new Texture(defaultDecal, this.scene);
    this.decalMaterial.diffuseTexture.hasAlpha = true;

    const handlePointerMove = throttle(() => {
      if (!this.canDecal) return;
      this.createDecal();
    }, 100);

    // 右鍵是否按下
    let rmbDown = false;
    this.scene.onBeforeRenderObservable.add(() => {
      if (!this.canDecal || !rmbDown) return;

      this.decalAngle += 0.025;
      this.createDecal();
    });

    this.scene.onPointerObservable.add((info) => {
      if (!this.canDecal) return;

      switch (info.type) {
        case PointerEventTypes.POINTERMOVE: {
          handlePointerMove();
          break;
        }

        case PointerEventTypes.POINTERWHEEL: {
          const event = info.event as WheelEvent;
          if (event.deltaY > 0) {
            this.decalSize += 0.05;
          } else {
            this.decalSize -= 0.05;
          }

          this.decalSize = Math.max(this.decalSize, 0.1)
          this.createDecal();
          break;
        }

        case PointerEventTypes.POINTERUP: {
          if (this.decal && info.event.button === 0) {
            const cloneDecal = this.decal.clone('decalCloned');
            cloneDecal.material = this.decal.material!.clone('decalMaterialCloned');
          }

          if (info.event.button === 2) {
            rmbDown = false;
          }
          break;
        }
        case PointerEventTypes.POINTERDOWN: {
          if (info.event.button === 2) {
            rmbDown = true;
          }
          break;
        }
      }
    });
  }
  private createDecal() {
    const pickResult = this.scene.pick(this.scene.pointerX, this.scene.pointerY,
      (mesh) => mesh.name === 'bottomShell'
    );

    if (this.decal) {
      this.decal.dispose();
      this.decal = undefined;
    }

    if (!pickResult.hit) return;

    const normal = this.scene.activeCamera?.getForwardRay().direction.negateInPlace().normalize();
    const position = pickResult.pickedPoint;
    const sourceMesh = pickResult.pickedMesh;

    if (!normal || !position || !sourceMesh) return;

    const size = new Vector3(this.decalSize, this.decalSize, 1);
    const angle = this.decalAngle;

    this.decal = MeshBuilder.CreateDecal('decal', sourceMesh, {
      position, normal, angle, size, cullBackFaces: false, localMode: true
    });
    this.decal.material = this.decalMaterial;
  }
  enableDecal(value: boolean) {
    this.canDecal = value;

    if (!value) {
      this.decal?.dispose();
      this.decal = undefined;
    }
  }
  setDecal(url: string) {
    this.decalMaterial.diffuseTexture = new Texture(url, this.scene);
    this.decalMaterial.diffuseTexture.hasAlpha = true;
    this.decalMaterial.diffuseColor = Color3.White();
  }

  private initTopShellAnimation() {
    const rise = new Animation(
      'rise',
      'position.y',
      this.FRAME_RATE,
      Animation.ANIMATIONTYPE_FLOAT,
      Animation.ANIMATIONLOOPMODE_CONSTANT
    );

    const riseFrames = [
      { frame: 0, value: 0 },
      { frame: this.FRAME_RATE, value: this.TOP_SHELL_MOVE_HEIGHT }
    ];

    rise.setKeys(riseFrames);

    const riseEasing = new QuarticEase();
    riseEasing.setEasingMode(EasingFunction.EASINGMODE_EASEIN);
    rise.setEasingFunction(riseEasing);

    set(this, 'animationMap.rise', rise);


    const fall = new Animation(
      'fall',
      'position.y',
      this.FRAME_RATE,
      Animation.ANIMATIONTYPE_FLOAT,
      Animation.ANIMATIONLOOPMODE_CONSTANT
    );

    const fallFrames = [
      { frame: 0, value: this.TOP_SHELL_MOVE_HEIGHT },
      { frame: this.FRAME_RATE, value: 0 }
    ];

    fall.setKeys(fallFrames);

    const fallEasing = new QuarticEase();
    fallEasing.setEasingMode(EasingFunction.EASINGMODE_EASEOUT);
    fall.setEasingFunction(fallEasing);

    set(this, 'animationMap.fall', fall);

    Animation.CreateAnimation
  }
  private setChanging(value: boolean) {
    this.isChanging = value;

    // 檢查目標是否與目前相等，若不相等則開始更換
    if (!value && this.targetShell) {
      const current = this.topMeshes.find(({ mesh }) => mesh.isEnabled());

      if (!current || current.name === this.targetShell) return;

      this.setShell(this.targetShell);
    }
  }
  async setShell(name: `${TopShell}`) {
    this.targetShell = name;
    if (this.isChanging) {
      return Promise.reject(`正在更換中`);
    }

    const current = this.topMeshes.find(({ mesh }) => mesh.isEnabled());
    if (!current) {
      return Promise.reject(`current 不存在`);
    }
    if (current.name === name) return;

    const target = this.topMeshes.find((item) => item.name === name);
    if (!target) {
      return Promise.reject(`目標不存在`);
    }

    this.setChanging(true);
    const riseAnim = this.scene.beginDirectAnimation(current.mesh, [this.animationMap.rise], 0, this.FRAME_RATE);
    await riseAnim.waitAsync();

    this.topMeshes.forEach(({ mesh }) => mesh.setEnabled(false));
    target.mesh.setEnabled(true);

    const fallAnim = this.scene.beginDirectAnimation(target.mesh, [this.animationMap.fall], 0, this.FRAME_RATE);
    await fallAnim.waitAsync();

    this.setChanging(false);
  }

  async setButtonColor(name: `${BtnColor}`) {
    const target = btnColorList.find((color) => color.name === name);
    if (!target) {
      return Promise.reject(`${name} 顏色不存在`);
    }

    const { diffuseColor, emissiveColor } = target;

    this.buttonMaterial.animations = [
      this.createAnimation(this.buttonMaterial, 'diffuseColor', diffuseColor),
      this.createAnimation(this.buttonMaterial, 'emissiveColor', emissiveColor),
      this.createAnimation(this.buttonMaterial, 'specularColor', emissiveColor),
    ];

    this.scene.beginAnimation(this.buttonMaterial, 0, this.FRAME_RATE, false);
  }
}
